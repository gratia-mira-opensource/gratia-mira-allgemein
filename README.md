# Gratia-Mira.ch

Dieses Projekt enthält grundlegende Dinge Entwicklung dieser Webseite, wie auch Anleitungen und mehr.

# Leitlinien beim Programmieren

* Funktionen sollen einen geistlichen Nutzen haben.
* Das Wort Gottes soll im Zentrum stehen und bleiben.
* Zwingende Admin-Aufgaben vermeiden.
* Da die Software auf diese Seite zugeschnitten sind, pragmatische Lösungen treffen.
* Code sauber dokumentieren.
* Konsequente Fehlerbeseitigung.
* In deutscher Sprache programmieren.
* Die Hauptgefahr durch der "digitalen Demenz" ist, dass man in den Gedanken schnell hin un herspringt. Dem soll trotz des Zieles von guter interner Verlinkung besser berücksichtigt werden.

# An welchen Dingen wird noch gearbeitet

Punkte, an welchen gearbeitet werden (nach Priorität geordnet).

- [ ] Neuer Schlachter Text einbinden.
- [ ] TBK Verzeichnis übersichtlicher & mit Suchformular.
- [ ] Code sauberer.

# Standards beim Programmieren

Einige Dinge, die ich immer gleich machen möchte, damit Fehler ausgeschlossen werden können.

## Links
Den Inhalt von Attributen werden immer mit doppelten Quote-Zeichen geschrieben z. B.: `href="link"`. 
Wann immer möglich, sollte man auf die `` Quote-Zeichen verzichten und nur die normalen, einfachen verwenden (`''`).

## Symbole
Symbole verwende ich wenn immer möglich von Font Awesome.