# Anliegen

Schwergewicht der Webseite sollen Parallelstellen, zufällige Bibelverse und Hilfestellungen zur Bibel bilden, wobei gute
andere Webseiten nicht konkurriert, sondern ergänzt werden sollen.  
Ausdrücklich sind folgende Seiten zu empfehlen:

* https://schlachterbibel.de/de: Diese Seite eignet sich, um einen längeren Abschnitt zu lesen, wenn die Bibel aus
  Papier nicht in Reichweite ist. Das Design ist viel ruhiger und somit auch übersichtlicher. Ein direkter Link ist
  unter dem Symbol des offenen Buches bereitgestellt.
* https://csv-bibel.de/start: Besonders hilfreich ist hier neben der guten Übersetzung die Grundtextfunktion. Durch
  Klicken auf ein Wort können zusätzliche Information über dasselbe erforscht werden. Ein direkter Link ist unter dem
  Symbol des geschossenen Buches bereitgestellt.

Mit diesen Hauptgebieten soll diese Webseite dem Herrn dienen - wenn es Ihm gefällt.

Themenübersicht:

[[_TOC_]]

# Stichwortsuche

Die Bibel besser kennenlernen. Diesen Prozess möchte ich unterstützen.  
[Stichwortsuche öffnen](https://gratia-mira.ch/j3/?Stichwort=suchen%2C+finden&Layout=Studienansicht).

Zur Stichwortsuche nutzt man das [Suchformular](#suchformular-bibel) auf
der [Studienseite](https://gratia-mira.ch/j3/?Layout=Studienansicht).
Die besten Suchresultate werden zurückgegeben, wenn der Suchbegriff mehr als zwei Stichworte ohne Füllwörter, z. B.
»Wohlgefallen Sohn Himmel« enthält.
Werden Zahlen gesucht, sind diese mit Nummern einzugeben (z. B. »50 und 10«).

Standardmässig ist die Suche auf »Automatisch« eingestellt.
Das Sichtwort durchläuft so verschiedenen Stufen.
Diese sind:

* Genaue Suche in der Bibel (mit zusätzlichen Stichworte vom Administrator).
* Genaue Suche in der Bibel ohne die Reihenfolge zu beachten. Alle Worte müssen vorhanden sein.
* Auch die Kommentare werden durchsucht. Alle Worte müssen vorhanden sein.
* Ungenaue Suche, d.h. andere Wortendungen werden zugelassen. Alle Worte müssen vorhanden sein.
* Die letzte Stufe ist die Einzelwortsuche. Sie tritt nur dann in Kraft, wenn alle anderen Stufen keine Treffer gebracht
  haben. Wie das Wort sagt werden alle Stellen zurückgegeben, die das Stichwort in ungenauer Form im Bibelvers,
  Bibelstelle, zusätzlichen Administrator Stichworten oder im Kommentar haben. Die zurückgegebenen Stellen werden anhand
  eines Logarithmus bewertet und das relevanteste Ereignis wird zurückgegeben.

In den [Einstellungen](#einstellungen) kann die Suche beeinflusst werden.

## Einstellungen

Klickt man auf das Zahnrad, können diverse Einstellungen gemacht werden. Klicken sie immer
auf Speichern, um die neuen Einstellungen zu übernehmen.

### Bibelteil

Legen Sie fest, aus welchen Teil der Bibel die Bibelstellen ausgegeben werden sollen.
Zur Auswahl steht »Altes Testament« und »Neues Testament« das die »Ganze Bibel«.
Diese Funktion wirkt sich auf Funktionen der ganzen Webseite aus.
Sie wird immer dann aufgehoben, wenn ein Leere Resultat angezeigt würde.

### Sucheinstellungen

Legen Sie hier fest, nach welchen Kriterien die Bibel durchsucht werden soll.

#### Automatisch

Alle Punkte werden wie unter [Stichwortsuche](#stichwortsuche) beschrieben durchlaufen.

#### Nur Bibel

Es werden nur Treffer angezeigt, die auch genau so in der Bibel (Schlachter2000) vorkommen.

#### Ungenaue Suche

Der Suchdurchlauf beginnt mit Punkt drei wie oben [beschrieben](#stichwortsuche).

#### Einzelworte

Der Suchdurchlauf springt direkt zu Punkt 5 wie oben [beschrieben](#stichwortsuche).

#### REGEXP (MySQL)

Diese Funktion ist im Beta-Zustand. Hier kann man mit regulären Ausdrücken suchen. Es wird nur im genauen Bibeltext
gesucht. Hier eine genaue Erläuterung: https://www.guru99.com/de/regular-expressions.html

### Funktionen via Parametereingabe

#### Die Suche einschränken

* `link/?Stichwort=möglich`
  Zeigt ein Bibelvers mit einem bestimmten Stichwort an.
  Das Stichwort kann mit den anderen Parameter kombiniert werden.  
  Wenn das Stichwort ein Bibelvers, Bibeltext, Stellenabfolge oder TBK-Liste beinhaltet,
  wird dies automatisch erkannt und gemäss dem entsprechenden Plan behandelt.

* `link/?BereichBibel=AT` oder `NT` oder `GanzeBibel`  
  Sucht den Begriff in ausgewähltem Bereich.
  Das Stichwort kann mit den anderen Parameter kombiniert werden.

* `link/?Bibelbuch=2. Könige`  
  Zeigt ein Bibelvers aus einem bestimmten Bibelbuch an.

* `link/?Kapitel=3`  
  Zeigt ein Bibelvers aus einem bestimmten Kapitel an. Es ist auch möglich hier Kapitel=3,16 einzugeben.

* `link/?Vers=16`  
  Zeigt ein zufälliger Bibelvers des bestimmten Verses an. Es ist auch möglich hier Kapitel=3,16 einzugeben.

* `link/?Bibelstelle=Markus 9,23`  
  Zeigt eine bestimmte Bibelstelle an. Es ist auch möglich hier 1. Mose 24,1-67 einzugeben.  
  Das Admin-Stichwort kann mit `Stichwort=~` überschreiben werden.

#### Die Suche einstellen

* `link/?Suche=ungenau` oder `genau` oder `REGEXP` Suchgenauigkeit kurzfristig ändern.

# Parallelstellen

Menschen sollen erkennen, dass die Bibel Antworten gibt, wenn man sie studiert. Ein spezielles Anliegen der Seite ist
der innere Zusammenhang der Bibel aufzuzeigen. Das Symbol dafür ist der Doktorhut, was von Fleiss spricht, wobei einer
unser Meister ist, wir
alle aber Brüder (
vgl. [Matthäus 23,8](https://gratia-mira.ch/j3/?Stichwort=Matth%C3%A4us+23&Plugin=Bibelstelle&Hervorhebung=Ihr%20aber%20sollt%20euch%20nicht%20Rabbi%20nennen%20lassen,%20denn%20einer%20ist%20euer%20Meister,%20der%20Christus;%20ihr%20aber%20seid%20alle%20Br%C3%BCder.#Fokus)).

[Parallelstellen-Ansicht öffnen](https://gratia-mira.ch/j3/?Stichwort=Psalm+119%2C68&BereichBibel=GanzeBibel&Layout=Studienansicht).

[Funktionsvideo](https://gratia-mira.ch/dokumente/Parallelstellen.mp4)

Da es oft viele Parallelstellen sind, gibt es diverse Möglichkeiten, die Auswahl einzuschränken. Diese Funktionen stehen
erst ab 10 Bibelversen zur Verfügung.

* Standardmäßig sind die Bibelstellen nach der biblischen Reihenfolge geordnet.
  So kann man unter »Zu Bibelbuch springen« einfach zum gewünschten Bibelbuch springen.
* Unter »Parallelstellen filtern« können die Parallelstellen nach einem freien Stichwort gefiltert werden.
  Dazu gibt man das Stichwort in das Textfeld ein und klickt auf das Lupen-Symbol.
  Gesucht wird das Wort im Vers, den zusätzlich vom Administrator hinzugefügten Stichworten und den Tags.
  Diese Filterfunktion ist in einem Beta-Entwicklung-Status.
* Nach und nach werden die Parallelstellen auch nach einem [Tag](#parallelstellen-tags) gefiltert werden, ähnlich wie
  man dies sich von der [TSK-Originalsammlung](https://www.tsk-online.com/)
  kennt. Auch diese Funktion ist in einem Beta-Entwicklung-Status. Zudem ist klar, dass jeder die Verbindungen zum
  Hauptvers etwas anders beschreiben würde. Dies ist einfach ein Versuch einer Hilfestellung.

Bei jeder Parallelstelle hat der Benutzer die Möglichkeit, sich wiederum die Parallelstellen desjenigen Verses
anzuzeigen. Auch der Direktlink zur Schlachterbibel, sowie zu Elberfelder CSV-Übersetzung sowie eine Teil- und
Kopierfunktion ist
jederzeit verfügbar. Mit dem Aufrufe-Zeichen kann man eine [Korrektur](#parallelstellen-korrekturvorschläge)
vorschlagen.

Ganz unten findet sich zudem die Abfolge der Bibelstellen als schön formatierte Angaben-Abfolge. Diese kann entweder in
einem neuen Fenster geöffnet und modifiziert werden oder als Vorlage für einen zufälligen Vers dienen (zwei Pfeile).

Es besteht auch die Möglichkeit, alle Stellen anzuzeigen, welche diesen Vers als Parallelstelle haben. Diese Funktion
ist in der Beta-Phase und wird mit dem Parameter `Parallelstellenspiegel=true` aktiviert (
z.B. [Prediger 9,10](https://gratia-mira.ch/j3/?BereichBibel=GanzeBibel&Layout=Studienansicht&Stichwort=Prediger+9%2C10&Parallelstellenspiegel=true)).

Geistliche und rechtliche Hinweise finden
Sie [hier](https://gratia-mira.ch/j3/index.php/gratia-mira-ch-mehr/allgemein/ueber-diese-parallelstellen).

### Parallelstellen-Tags

Parallelstellen-Tags oder auch "Hilfe-Tags" dienen dazu, eine Hilfestellung darüber zu geben, welche Beziehung zwischen
der Parallelstelle und
dem Hauptvers besteht.

Grundsätzlich wird dazu ein Wort aus dem Vers ausgewählt, welche diese Beziehung kennzeichnet.
Darüber hinaus oder in Kombination können unter anderem folgende Begriffe auftauchen. Die grauen Tags beziehen sich
immer auf das letzte grüne Wort und bilden eine Selektion desselben.

Hier die Übersicht:

| Begriff        | Bedeutung                                                                                                                                                       |
|:---------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Jesus Christus | Es geht um unseren Erretter, Hirten und Herr. Vorallem in einer Prophetie.                                                                                      |
| Ankündigung    | Etwas was in der Parallelstelle angekündigt wird und in der Hauptstele in Erfüllung geht. Gegenstück: Erfüllung                                                 |
| Beispiel       | Es ist ein Beispiel dessen, was der Hauptvers ausdrückt. Dies kann auch ein bildliche Darstellung im Alten Testament sein. Verwandt: Praxis                     |
| Erfüllung      | So wird z. B. eine erfüllte Prophetie oder Bitte bezeichnet, die im Hauptvers angekündigt wird. Gegenstück: Verheißung                                          |
| Folge          | Der Vers enthält Hinweise darauf, was die direkten Folgen einer Aussage oder Handlung des Hauptverses sind. Gegenstück: Vorgeschichte                           |
| Gefahr         | In diesem Vers wird auf eine Gefahr für unser Glaubensleben hingewiesen, die mit der Hauptvers in besonderer Verbindung steht.                                  |
| Gegensatz      | Der Vers steht in einem hilfreichen Gegensatz, bzw. Kontrast. Verwandt: Sichtweise                                                                              |
| Hintergrund    | Bezeichnet, das er Vers Hintergrundinformationen liefert, weshalb oder wieso sich etwas so verhält.                                                             |
| Konsequenz     | Beschreibt eine direkte Konsequenz des Hauptverses; kann auch eine Verheißung sein. Gegenstück: Ursache                                                         |
| Praxis         | Der Vers beschreibt wie der Hauptvers in der Praxis unseres Glaubenslebens aussehen könnte oder was uns im Helfen könnte, dazu hinzuwachsen. Verwandt: Beispiel |
| Sichtweise     | Beschreibt die selbe Sache aus den Augen einer andern Person oder Zielsetzung.                                                                                  |
| Ursache        | In diesem Vers wird die Ursache, Warnung oder Quelle des Hauptverses beschrieben. Gegenstück: Konsequenz.                                                       |
| Vergleichbar   | In seltenen Fällen sind zwei Verse wirklich ähnlich.                                                                                                            |
| Verheißung     | Der Vers zeigt eine Verheißung auf, welche im dem Hauptvers erfüllt wird. Gegenstück: Erfüllung                                                                 |
| Voraussetzung  | Dieser Vers enthält Voraussetzungen des für den Hauptvers. Gegenstück: Folge                                                                                    |
| Vorgeschichte  | Die entsprechende Stelle ist zeitlich vor dem Hauptvers geschehen und beeinflusste ihn möglicherweise.                                                          |
| Ziel           | Im entsprechenden Vers wird das Ziel dessen beschrieben, welches der Hauptvers erreichen möchte.                                                                |
| ?              | Alle Stichworte wiederspiegeln die Ansicht der Bearbeiter. Bei diesen gilt es aber den Zusammenhang besonders gut zu prüfen.                                    |

### Parallelstellen-Korrekturvorschläge

Durch Klicken auf das Ausrufezeichen wird eine anonyme Benachrichtigung an den Seitenadministrator gesandt, da ansonsten
viele gute Rückmeldungen nicht gemacht werden.
Diese Mitteilung lässt jedoch keine Rückschlüsse auf die Person zu, welche sie ausgelöst hat.

Noch hilfreicher ist es jedoch, kurz mitzuteilen, wieso eine Korrektur vorgeschlagen wurde.
Hier kann auch eine neue Parallelstelle vorgeschlagen werden oder die Löschung beantragt werden.
Auch hier wird der Administrator informiert und bekommt die Möglichkeit, die entsprechende Korrektur freizugeben.
Notizen und Kommentare bleiben immer anonym und werden nicht veröffentlicht.

## Eine bestimmte Parallelstelle öffnen

Immer wenn das Doktorhut-Icon angezeigt wird, kommt man zur Parallelstellensammlung, der entsprechenden Stelle. Wurde
ein Bibeltext gewählt, kann man einfach auf die gewünschte Stelle klicken, um zur gewünschten Ansicht zu kommen. Auf
einem Desktopcomputer wird der Hauptvers rechts angezeigt. Links daneben befinden sich die Parallelstellen zum
entsprechenden Vers.

Man kommt auch zu den Parallelstellen, wenn man im [Suchfeld](#suchformular-bibel) eine gültige Bibelstelle eingibt.
Wenn man nicht sicher ist,
wie ein Bibelbuch geschrieben wird, kann man durch Klicken auf das Suchfeld ein Bibelbuch auswählen.

# Themenbibelkonkordanz

Es soll nicht nur eine Parallelstellensammlung angezeigt werden, sondern auch thematische Zusammenstellungen.

Stellen über ein bestimmtes Thema aus Gottes Wort. Sie sollen eher hirtencharakter haben und nicht unbedingt
wissenschaftlicher Art sein.

[TBK öffnen](https://gratia-mira.ch/j3/bibelverse/themen-bibel-konkordanz).

# Zufällige Bibelstellen

Man soll die Bibel mit in den Alltag nehmen.
In diesen Bereich gehören auch die Gedankenanstöße.
Es ist ein Anliegen, die Bibel so wiederzugeben wie sie ist.
Deshalb werden auch alle möglichen 31'171 Verse angezeigt.

In wenigen Tagen im Jahr kommt es vor, dass ein tagesaktueller Filter angewendet wird.
Dies trifft immer nur dann zu, wenn keine anderen Filter gesetzt sind.
Zusätzlich wird der Vers mit einer Nachricht unteren Bildschirmrand näher beschrieben.
Durch Klicken auf »Tagesfilter löschen« werden wieder alle Verse angezeigt.

[Zufälligen Vers anzeigen](https://gratia-mira.ch/).

## Einstellungen

[Siehe Einstellungen bei der Stichwortsuche.](#einstellungen)

## Funktionen via Parametereingabe

### Die Seite neu laden

* `link/?NeuladenZeit=10` (10 Sekunden ist das Kleinste).

### Die Suche einstellen

* `link/?Suche=ungenau` oder "genau" oder "REGEXP" = Suchgenauigkeit kurzfristig ändern.

### Nur kommentierte Verse anzeigen

* `link/?Kommentare=Ja` oder Nein (Nein bedeutet, dass die Kommentare nicht angezeigt werden).

### Ein Vollbildlayout auswählen

* `link/?Layout=Vollbild`

### Tagesfilter steuern

Mit `link/?Tagesvers=Nein` können die Tagesfilter manuell unterdrückwerden. Mit Ja werden sie wieder angezeigt.
Achtung: Es gibt nur wenige Tage im Jahr, bei welchen ein Tagesvers / Stichwort zugeordnet ist.

# Kurzgedanken

Dieser Teil hängt eng mit den zufälligen Bibelversen zusammen und entstand dadurch, dass mir jemand sagte, dass manche
Verse wohl alleinstehend schwer zu verstehen sind. Die Verse sollen ohne die Kommentare wahrgenommen werden, dennoch
sollten die Kommentare dazu beitragen, manchmal vertieft und in einer ungewohnten Art darüber nachzudenken.

[Zufälliger Bibelvers mit Kommentar öffnen](https://gratia-mira.ch/?Kommentare=Ja#Kommentar).
[In den Kommentaren stöbern](https://gratia-mira.ch/j3/gedankenanstoesse/alle-beitraege).

# Weitere Funktionen

Diese Funktionen hat die Webseite auch zu bieten, sie sind aber nicht das Hauptanliegen.

## Bibelteile anzeigen oder abspielen

Selbstverständlich kann jeder beliebige Bibelvers oder Bibeltext angezeigt werden. Dazu können Sie einfach die
Bibelstelle oder den Bibeltext in der Suche eingeben.

Wird ein Bibeltext angezeigt, werden beim Klicken auf die gewünschte Bibelstelle direkt die dazugehörigen
Parallelstellen angezeigt.

Sie können aber auch Bibeltexte abspielen. Zum Beispiel einen wertvollen Text über unseren Herrn Jesus
Christus ([bitte hier klicken](https://gratia-mira.ch/j3/?Modus=auto&BereichBibel=GanzeBibel&Bibelstelle=Jesaja%2052,13&Zielvers=Jesaja%2053,12&Stichwort=~&NeuladenZeit=10&Layout=Vollbild#Vers)).

Wie Sie selber eine solche Anzeige erstellen, können Sie in den Funktionen für Fortgeschrittene nachlesen.

### Funktionen für Fortgeschrittene

#### Den Modus wählen

Wenn ein Modus gewählt ist, kann die Suche nicht mehr eingeschränkt werden.

* `link?Modus=vor` = Es wird bei jeder Aktualisierung der Seite der nächste Bibelvers angezeigt.
* `link?Modus=zurueck` = Es wird bei jeder Aktualisierung der Seite der vorherige Bibelvers angezeigt.

#### Einen Bibeltext abspielen

* `link/?Bibelstelle=2. Mose 12,1&Modus=auto&NeuladenZeit=15&Zielvers=2. Mose 12,12`

#### Einen Bibeltext anzeigen

* `link/?Bibeltext=1. Mose 24,1-67`

#### Ein Vollbildlayout auswählen

* `link/?Layout=Vollbild`

#### Nur kommentierte Verse anzeigen

* `link/?Kommentare=Ja`  oder Nein (Nein bedeutet, dass keine Kommentare angezeigt werden).

## Bibelstellenabfolge anzeigen

Gibt man in das Suchfeld eine Bibelstellenabfolge ein, wird diese ausgegeben. Dabei wird keine Änderung an der
Reihenfolge der Bibelstellen gemacht.

[Bibelstellenfolge anzeigen.](https://gratia-mira.ch/j3/?Stichwort=5Mo%2032,4;%20Ps%2016,2;%20119,65.68;%2086,5;%2025,8;%20100,5;%20109,21;%20111,3;%20Zeph%203,5&Layout=Studienansicht)

### Funktionen via Parametereingabe

#### Bibelstellenabfolgen sortieren

Die Bibelstellenabfolge kann gemäss biblischer Reihenfolge sortiert werden.

* `link?Sortierung`

## Bibelvers-Spiel

Wie gut kennen Sie die Bibel. Prüfen Sie sich selbst.
[Zum Bibelvers-Spiel](https://gratia-mira.ch/j3/index.php/gratia-mira-ch-mehr/bibelvers-spiel#Main).

## Passwortvorschläge

Mit folgendem Code kann man sich Passwörter mit einem biblischen Bezug vorschlagen lassen.  
Das Passwort enthält 10-19 Zeichen aller Art und kann als «einigermassen» sicher eingestuft werden.  
Die Funktion funktioniert nur in der Standartansicht.  
Man kann auch ein Stichwort mitgeben, indem man den Parameter `Stichwort=Wort` verwendet. Es werden nur einzelne Wörter
akzeptiert.
[Passwortvorschläge anzeigen](https://gratia-mira.ch/?Passwort#Passwort).

## Suchformular Bibel

Dieses Formular findet sich z.B. in der [Studienansicht](https://gratia-mira.ch/j3/?Layout=Studienansicht), oder auf der
Seite [Ihr Stichwort](https://gratia-mira.ch/j3/bibelverse/zu-ihrem-stichwort).

Das Suchformular der Webseite prüft das Stichwort und wählt je nachdem die Ausgabebedingungen. Es wird unterschieden,
zwischen einer [Bibelstelle](#bibelteile-anzeigen-oder-abspielen), einem Bibeltext,
einer [Versabfolge](#bibelstellenabfolge-anzeigen), eine [Themen | Bibel | Konkordanz-Liste](#themenbibelkonkordanz)
oder einem [Stichwort](#stichwortsuche). Alles kann über dasselbe
Formular eingegeben werden.

Wurde ein Stichwort eingegeben, wird das Feld »Suche einschränken« angezeigt. Hier kann man diverse Filter erstellen.
Möchte man z. B. nur Johannesevangelium suchen, reicht
die Eingabe von `Joh`. Möchte man die Suche auf Matthäus 13 einschränken, reicht `Mt 13`. Es sind jedoch auch zwei
Kapitel, wie `Mt 13; Joh 1` möglich oder gar einzelne Verse, die durch einen Strichpunkt getrennt sind. Durch Klicken
auf das Feld, werden einige Vorgaben angezeigt.

Unter dem Suchformular werden vor angepasste Suchvorschläge angezeigt.

Eine Übersicht über die möglichen Einstellungen finden sich [hier](#einstellungen).

# Entwickler

Hinweise für Entwickler.

### Fenster

"Fenster" (in Anlehnung an Frame). Soll eine Vollbildansicht, die sich z.B. für Organisationen oder Firmen im Design
anpassen lässt?

Parameter:

* `link/?Layout=Fenster`
* `&Schriftfarbe={HEX-Farbcode}`
* `&Hintergrundsfarbe={HEX-Farbcode}`
* Alle anderen Parameter in Ergänzung möglich

Hier eine Liste mit möglichen HTML-Farben, wobei nur HEX-Farbcode unterstützt werden:
https://www.computerhope.com/htmcolor.htm